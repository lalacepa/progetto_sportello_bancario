let n5, n10, n20, n50, n100, n200, n500;

//Questa funzione legge il numero di banconote inserite,le somma ma poi
// le moltiplica anche per il loro corrispondente valore monetario, restituendone la somma.
//al termine la pagina viene reinderizzata sulla nuova pagina "Riepilogo_Banconote".
function Versamento(valore) {
    
n5 = document.getElementById("cinque_euro").value;
n10 = document.getElementById("dieci_euro").value;
n20 = document.getElementById("venti_euro").value;
n50 = document.getElementById("cinquanta_euro").value;
n100 = document.getElementById("cento_euro").value;
n200 = document.getElementById("duecento_euro").value;
n500 = document.getElementById("cinquecento_euro").value;

let N = parseInt(n5)+parseInt(n10)+parseInt(n20)+parseInt(n50)+parseInt(n100)+parseInt(n200)+parseInt(n500); //Numero totale delle banconote inserite
localStorage.setItem("NumeroBanconote", N);
               
let somma = (n5*5.00)+(n10*10.00)+(n20*20.00)+(n50*50.00)+(n100*100.00)+(n200*200.00)+(n500*500.00);
localStorage.setItem("TotaleVersato", somma);

setTimeout(function() {  
 window.location.assign("/Riepilogo_banconote.html");}, 5000);
}

//Queste due funzioni vengono eseguite al completamento del caricamento della pagina "Riepilogo_Banconote",
//per passare i seguenti parametri: 1) il numero totale di banconote inserite; 2) il totale versato;
function MostraUno() {
    if (document.getElementById("banconote_inserite")!= null) {
        document.getElementById("banconote_inserite").innerHTML = "banconote inserite:" + " " +localStorage.getItem("NumeroBanconote");
        }}

function MostraDue () {
    if (document.getElementById("totale_versato")!= null) {
        document.getElementById("totale_versato").innerHTML = "Totale versato:€" + " " +localStorage.getItem("TotaleVersato");
        }}

//Questa funzione mostra il totale versato nella pagina "Hai_Versato_banconote"
function VersaSuConto() {
    if (document.getElementById("hai_versato")!= null) {
        document.getElementById("hai_versato").innerHTML = "€" + " " +localStorage.getItem("TotaleVersato");
}}

function MostraInCausaleBanconote() {
    if (document.getElementById("importo_totale_banconote")!= null) {
        document.getElementById("importo_totale_banconote").innerHTML = "Importo totale banconote:€" + " " +localStorage.getItem("TotaleVersato")+",00";
        }
if (document.getElementById("importo_totale_assegni")!= null) {
document.getElementById("importo_totale_assegni").innerHTML = "Importo totale assegni:€" + " " +"0,00";
}
}