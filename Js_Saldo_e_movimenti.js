function green(){
                document.getElementById("dropdownMenuLink").style.color= "rgba(12, 158, 134, 0.973)";
}

// Funzione che consente il Logout dalla pagina saldo
function Logout() {
    if (confirm("Sei sicuro di voler uscire?") == true) {
        window.location.assign("/Area_saldo_pin_conti.html");
    } else {
        window.location.assign("/saldoMovimenti.html");
        }
}

//Funzioni che mostrano e nascondono il saldo
function mostra() {
    document.getElementById("saldo-nascosto").innerHTML = localStorage.getItem("saldoIniziale") +" "+ "€";
}

function nascondi() {
    document.getElementById("saldo-nascosto").innerHTML = "Saldo nascosto ";
}

//Funzioni che conservano il saldo in una variabile e la recuperano per mostrarla nella pagina del prelievo
if (document.getElementById("disponibilità")!= null) {
    if (localStorage.getItem("saldoIniziale")) {  
        document.getElementById("disponibilità").innerHTML = "Disponibilità: €" + " " +localStorage.getItem("saldoIniziale");
    } else {
            document.getElementById("disponibilità").innerHTML = "Saldo non disponibile";
            }
}

//Saldo di partenza
function caricaSaldo() {
    localStorage.setItem("saldoIniziale", 2456.63);
    document.getElementById("disponibilità").innerHTML = "Disponibilità: €" + " " +localStorage.getItem("saldoIniziale");
}

//Sottraggo l'importo dal saldo nella pagina prelievi, aggiorno la disponibilità e
//l'importo scelto nella pagina seguente
let importo;
    function valor() {
        importo = document.getElementById("display-importo").innerHTML;
        console.log(typeof(importo));           //typeof: String
        localStorage.setItem("scelta", importo);     //typeof: String
            if (confirm ("Premi OK per confermare il tuo importo: € " + importo) == true) {
                if (localStorage.getItem("saldoIniziale") == 0) {
                    localStorage.setItem("scelta", 0);
                    document.getElementById("disponibilità").innerHTML = "Disponibilità esaurita!";
                    document.getElementById("disponibilità").style.color = "red";
                    document.getElementById("messageOfError").innerHTML = "Saldo azzerato. Cassa contante. Potrebbe verificarsi un saldo negativo.<br>Prelievo non consentito! Si consiglia di ripristinare il saldo.";
                    setTimeout(function() {  
                    alert ("Premi 'OK' per essere reinderizzato nella pagina 'Tutte le operazioni'.");
                    window.location.assign("/tutte_le_operazioni.html");}, 4000);   
                    } else {
                        localStorage.setItem("saldo", localStorage.getItem("saldoIniziale")); //Salvo il valore del saldo prima che subisca mutamenti, serve per l'aggiornamento movimenti
                        saldoRecuperato = localStorage.getItem("saldoIniziale") - importo;
                        localStorage.setItem("saldoIniziale", saldoRecuperato);
                        alert("il tuo nuovo saldo è: € " + saldoRecuperato + ". " + " Premi 'Conferma' per concludere l'operazione");
                        document.getElementById("disponibilità").innerHTML ="Disponibilità: €" + " " + saldoRecuperato;
                        }
} else {
    window.location.assign("/Quanto_vuoi_prelevare.html");
    }
}

//utilizzo la variabile scelta per aggiornare la pagina di conferma prelievo
if (document.getElementById("contenitore")!= null) {
    document.getElementById("contenitore").innerHTML = localStorage.getItem("scelta");
}

//Il tasto 'Conferma' dovrà permettermi di:
//1)Navigare nella pagina di 'Conferma Importo' aggiornata dell'importo scelto nella pagina di Quanto_vuoi_prelevare

//1) function A:navigo nella pagina "Conferma importo" grazie al bottone "Conferma"
var click = 0;
    function premiConferma() {
    click = click + 1;
    var nPrelievi = click;
    const message = document.getElementById("messageOfError");
    message.innerHTML= "";
    let somma = document.getElementById("display-importo").innerHTML;
        try {
            if (nPrelievi<4 && somma == "") throw " empty"; 
                else if (nPrelievi>3){
                document.getElementById("messageOfError").innerHTML ="ATTENTION! You have exceeded the number of attempts!<br>Cause security reasons you cannot continue with the operations!";
                }
                    if (nPrelievi<4 && somma != "") {
                        window.location.assign("/Conferma_prelievo.html");
                    }
            }
            catch(err) {
                message.innerHTML="ATTENTION! The amount is "+ err; 
                }
    }




//2)In seguito alla navigazione nella nuova pagina, il codice eseguirà l'Aggiornamento
//della cronologia dei movimenti nella pagina SaldoMovimenti: cioè si rimuoverà l'ultimo 
//elemento della cronologia, si farà scalare di un posto tutti gli elementi e si aggiungerà
//come ultimo il movimento più recente.
var listaDaAggiornare = false;
var importoPrelevato = localStorage.getItem("scelta");      //typeof: String
                    
    if(importoPrelevato != 0) {
    localStorage.setItem(listaDaAggiornare, true);
    var listaAggiornare = localStorage.getItem(listaDaAggiornare);
    } 
    else { var listaAggiornare = listaDaAggiornare;
    console.log(listaAggiornare);
    }

var listaMovimenti = listaAggiornare;
var saldoS = localStorage.getItem("saldo"); //Riprendo il valore del saldo da cui parto per il singolo prelievo,                                            // è il secondo discriminante per ciclizzare gli aggiornamenti dei movimenti
console.log(saldoS);

        if (listaMovimenti!= false & saldoS == 2456.63) {
            
            if (document.getElementById("movCinque")!= null) {
                var sostitutoUno = document.querySelector("#movCinque").innerHTML;
                var sostitutoDue = document.querySelector("#movQuattro").innerHTML;
                var sostitutoTre = document.querySelector("#movTre").innerHTML;
                var sostitutoQuattro = document.querySelector("#movDue").innerHTML;
                var sostitutoCinque = document.querySelector("#movUno").innerHTML;      
            }
        if (document.getElementById("movSei")!= null) {
                document.getElementById("movSei").innerHTML= sostitutoUno;
                document.getElementById("movCinque").innerHTML= sostitutoDue;
                document.getElementById("movQuattro").innerHTML= sostitutoTre;
                document.getElementById("movTre").innerHTML= sostitutoQuattro;
                document.getElementById("movDue").innerHTML= sostitutoCinque;

                const d = new Date(); 
                let day = d.getDate();
                let month = d.getMonth() +1;
                let year = d.getFullYear();

                var dateWrapper = moment(d); //Per clonare l'oggetto DATE da spostare nel penultimo movimento
                console.log(dateWrapper);
                var daywrapper = moment().get('date');
                localStorage.setItem("dayClone",daywrapper);
                console.log(daywrapper);
                var monthwrapper = moment().get('month') + 1; 
                localStorage.setItem("monthClone", monthwrapper);
                console.log(monthwrapper);
                var yearwrapper = moment().get('year');
                localStorage.setItem("yearClone", yearwrapper);
                console.log(yearwrapper);

                var importoCopia = importoPrelevato; //clona l'importo, che è un typeof: String
                localStorage.setItem("importo_clonato", importoCopia);

                document.getElementById("titU").innerHTML= day+" " +"/"+" "+ month+" " + "/"+" "+ year;
                document.getElementById("descrU").innerHTML= "Prelievo contanti <br>" + "Carta 1234***************2345";
                document.getElementById("costU").innerHTML= "-"+ " "+ importoPrelevato +" " + "€" ;
                document.getElementById("imU").src= "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8XuXgAtnL4/fsPu3ua3b+e3cAAtW8AtW4At3T1/PkZvH0Jt3X7/v3m9u+f3sLI69uu48vd8+rO7uCS2bm45tFKw47X8ebt+vXj9u510alszaJTx5U6wYi+6daV27xgypuD1bB90qtaypqx48xNxpFx0KZnyp05wosyvoKL2LZuky4qAAAJsklEQVR4nO2da1fqOhCGbVDbQgoCoiiiCNst+P9/4MHLPsrMG5opMwXXyvO5tJk2mcw1nJ0lEolEIpFIJBKJRCKRSCQSiUQikUgkToCq8ztoKt94uT7/HayX4ybfb5Y7n/0OvMtnlVjCB3fscYtwD1IBL/Jjj1lIfiETsNM79ojF9GQKZ1wce8Bicpm2Gf6uVfiOG4okHPxCCQciCS9/oYSXScIk4amTJEwSnj5JwiTh6ZMkTBKePmoSuiMTjB2pSXhxXAY3IRGVJCxfRLcxIOibK0noro0GHs1daSthfmU08HjmgWmqJGHRNRp3PNeh6aUiod8YDVtAKAyoI6FbGg1bgukszWUROxuWgbevImFxZzRqCY844aAjoTBybkMX7xcqEvpXo0HLeIUiqkjonozGLGMIp6mKhMLkhxXYrFGR8LxvNGYh0PrWkNDfGI1YCjRrNCR097sXdQbMuZloCNB/orclKnxsJWFOxn9XUPe0ULF5+uy2t+QKtIRUvuFo96IJ02lOmEwPsKkb/D0YnoqmIRe9sYvoV27IiijLku7DyKxRkNCvyEV852XTqRns1fVItUzH5hvSNHK1YUq71NlO2PQvyPo4W/D9QkHCguz3Xe6obXQc5FsqIatCAGaNgoQ5mSpX7ClqGyZ9d2wh9vn4hBKCWgw2fH6NXxwi1g96dBJ6egU3a4S1GHwjyNwbueYvUzTu+RCxfsB0GFuI3KwR6vE+l5Ddgb1o6WsMc0HH7/6QK265hEItR7ek7UPoHbiiUYs1silUzsgVFbUKSrqX1cHekZ8T4/CWS+h0tsN3e5A+nQX56BqRP3vidm/B3iJYql7Lu+qe01uXdCHumjWle5Q/5Xbl8p/WL11jz1zd9ppKxGCqkmmB6ofZn7tVs9nTnwwu/+eJfp8XtlL9vOaG09uryfBy+DimH4RxT29OPbftZnXxb2iDiYlnDmps926H1eR+s33b+ccrd/7lYu9bf2JqYC0v5D6QEQ+WsB3zm/HM7RbGl67YXE6D13N7ialyc/gYwjWsVy8FCB55lz2HZOTvT8kxEwBiJTnWZ9OVC+b8fMBGmLI1oGYvRQPCssy0+mCIvt/3W1nj2ceUaa0a06Zacw+tADH/allTE+9L+OW50Zu3nLqcgg9T8suqVX3fBkxoccclsAbMAHlKv+aXvcTU5eTAswPKNKypTQAOJLPrzs4WcYVHBf+Kd9z5bHkhgjQlr9O4jm0togGSM5Tpde0uRJA6YNvhOLp3yvfY6Lkmc+3WgaA4Dh0Bd5GDODbD+SRpdyGOwB5QkJ1N1P7GXg+vfWq3Hgs4h5nfnWldgXzb4VNFDKzCXtiO1QfYbH6zu+ELW6cK8hFBNLbVBC13DlmgJFSdFYB9RO6dtVpUh4a466PyYFgNtJKFx8LKRXsCgpAzVXXMh62DZuZ4Zis7b6/YBagBal7ikpA9UFUJCklzrVhePTCtsaMHqgaNxLuqEkUrZamJQ5ghz2LHO+zIe8FJxBN4L+Kob3NQucfubsViuvXku5H7DlDGri0Bpyxgy0z/Bs3g1Cp7BT52WwuRZTC3lLuHGsRb3d8SkkIOoEzVUj91oMKy8u/OJUBPSCUElmFrOyII6NOqvmkDCUk0DdkMenmD/TwAVUrimRVYqnUSkr1giq5pZyHCQzMcMYvRW9gPc6B4rYf04ISm9NEMpAVToaaBMJ6WWK+4hCAWZAFyDrOSZE6Qvt0vIAvVIReN5mltgEY10wE3wmnKTTJY29VKLT0oSAIhBvil91AwDx7tOO30QyA1yTOYQhcYNasACVvpaQHxBWj2izZ9EE+EwbpW+pKQcwjTe2+CeQrTg8iF4ZNZH9jjCKLW0UH9dwFhdhC6oS1kSnniK/RqO+tIER12/KAybWEhQg0CMmvvvERN1HyBf41MC29vmsJIbygxVC0jRMRT9Cxg3NovRBgmDBea/ClrZqorwysLhGVbyJTCnXxPHUH31e2RsXR/9yTNlkBC+5IF5Bzuj4KNZiU+KMC7cra3PAoW1JtnSnksOqvV4f2ndUFLTkpXrC9qyoDg1psZ1w51gdcWE6vtD2a9ovis+srzojifDepHynPdmX3HPArobz9IlILrjK4mk8Hgz+RqFOcEdWmd7DvWCxFPHKU2BAaKFJTio0plQOfQrG8PNTjRVKw20GYzaxCGaVZoA+sBbTazpYE7DU3bkXHGxSwWDVt+bRcizkfYKXD0NNsEDT4Axy5QiyxT24UYOJTCzMyQ24iHAspKt2zMophwzpguRKhoDI1hvO69XeH+CEpomPSCgT2WJFEEn5xiaSnCHJZhphTkZTPbjBB01tRaOSOfZ+rP4ByWlaUPU3qZbQFB4KgWqyfeoafhNgQtcJbObCEGKiwsrSh8ZhKpi9AjcLiSZWwIta7wYlY1oHPIu+VVwSWAVqeO4aM1bStb8QZllCnl7bmfEprmZXEu2Wghogr99/f5WHU/UTQXq0632+lUgYhilp3rPeoHeG/Kst4/5qvlcHS4mP3J2+v6/X6b9WI5wJWqtPdBB7wkfuBLl7v580HbcX9w43PnP1a8394wdJKqSaY0rs7Ju3zd+PGj2D/SKnlphALRxRU+nzfyb/qzvQ2nO4+wKFmA1V6hV5zfy/fkYSYoFWNFYgoEjtUM4DbSIcxE97fIlLIzVWpesuysis5cdnuLkgVQK1gzCIGI3RthMaNFrkTeQiFw4+Jrb/4Rl9GTgNq3a/BZ7Cie5JXv+gsRZklqcIu4ewfswf33Vu8pbfRvc5GmB47/7Ee/pxRmK+uI846bfMLt29P2goP/u7B/GDGrBWYn6m+tHOLjx41EERUPF/abfqHteY8a/jFLUX/rQAivDnRMxSFI67b/l7B+TxR31H7hdBdirXMYkrDeyxB3LnyhvBAXDRT6h4SG31C1ZKGDA/oREta7GOBUrSh0M6UNmtG+JIy4d9N/H1Y9ZUHmHH4TteU322qVm7ub/n+uf+DnmrM/cXpoKKHS6dqfsHP+okU85I+4alBtZcNFGEdGtZWt6Se0xSmqmtP8K3LF6qjGm4UtikmhZh6cOYpWTVPT0RjFSp4T/YaK2QtJRL9FNDfE05RQM/Dd1Di2RTOQgcswjo1mwULTIIYt+NjbZsiO0GsJP9cspW0W07RFN57YFRxl2Rq66Sfc03VMCu3s06Sxp2pDoenhfzLundBndCbFZtVTmZ/EvuidezXqYukOV73cubL0x5mx3pely8v5m+JGyJiOB8+zl/mmd94675Vuq+X1pJVjeKqq6hyBqvW/uUgkEolEIpFIJBKJRCKRSCQSiUQikUjE8h/PNKFDoWQnRwAAAABJRU5ErkJggg==";
            } 
        } else if (listaMovimenti!= false & saldoS != 2456.63) {
            
                if (document.getElementById("movCinque")!= null) {
                    var sostitutoUno = document.querySelector("#movCinque").innerHTML;
                    var sostitutoDue = document.querySelector("#movQuattro").innerHTML;
                    var sostitutoTre = document.querySelector("#movTre").innerHTML;
                    var sostitutoQuattro = document.querySelector("#movDue").innerHTML;
                    var sostitutoCinque = document.querySelector("#movUno").innerHTML;         
                }
                    if (document.getElementById("movSei")!= null) {
                    document.getElementById("movSei").innerHTML= sostitutoDue;
                    document.getElementById("movCinque").innerHTML= sostitutoTre;
                    document.getElementById("movQuattro").innerHTML= sostitutoQuattro;
                    document.getElementById("movTre").innerHTML= sostitutoCinque;
        
                    //Questo snippet mi scala il primo aggiornamento nel secondo movimento
                    document.getElementById("titD").innerHTML= localStorage.getItem("dayClone")+" " +"/"+" "+ localStorage.getItem("monthClone")+" " + "/"+" "+ localStorage.getItem("yearClone");
                    document.getElementById("descrD").innerHTML= "Prelievo contanti <br>" + "Carta 1234***************2345";
                    document.getElementById("costD").innerHTML= "-"+ " "+ localStorage.getItem("importo_clonato")+" " + "€" ;
                    document.getElementById("imD").src= "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8XuXgAtnL4/fsPu3ua3b+e3cAAtW8AtW4At3T1/PkZvH0Jt3X7/v3m9u+f3sLI69uu48vd8+rO7uCS2bm45tFKw47X8ebt+vXj9u510alszaJTx5U6wYi+6daV27xgypuD1bB90qtaypqx48xNxpFx0KZnyp05wosyvoKL2LZuky4qAAAJsklEQVR4nO2da1fqOhCGbVDbQgoCoiiiCNst+P9/4MHLPsrMG5opMwXXyvO5tJk2mcw1nJ0lEolEIpFIJBKJRCKRSCQSiUQikUgkToCq8ztoKt94uT7/HayX4ybfb5Y7n/0OvMtnlVjCB3fscYtwD1IBL/Jjj1lIfiETsNM79ojF9GQKZ1wce8Bicpm2Gf6uVfiOG4okHPxCCQciCS9/oYSXScIk4amTJEwSnj5JwiTh6ZMkTBKePmoSuiMTjB2pSXhxXAY3IRGVJCxfRLcxIOibK0noro0GHs1daSthfmU08HjmgWmqJGHRNRp3PNeh6aUiod8YDVtAKAyoI6FbGg1bgukszWUROxuWgbevImFxZzRqCY844aAjoTBybkMX7xcqEvpXo0HLeIUiqkjonozGLGMIp6mKhMLkhxXYrFGR8LxvNGYh0PrWkNDfGI1YCjRrNCR097sXdQbMuZloCNB/orclKnxsJWFOxn9XUPe0ULF5+uy2t+QKtIRUvuFo96IJ02lOmEwPsKkb/D0YnoqmIRe9sYvoV27IiijLku7DyKxRkNCvyEV852XTqRns1fVItUzH5hvSNHK1YUq71NlO2PQvyPo4W/D9QkHCguz3Xe6obXQc5FsqIatCAGaNgoQ5mSpX7ClqGyZ9d2wh9vn4hBKCWgw2fH6NXxwi1g96dBJ6egU3a4S1GHwjyNwbueYvUzTu+RCxfsB0GFuI3KwR6vE+l5Ddgb1o6WsMc0HH7/6QK265hEItR7ek7UPoHbiiUYs1silUzsgVFbUKSrqX1cHekZ8T4/CWS+h0tsN3e5A+nQX56BqRP3vidm/B3iJYql7Lu+qe01uXdCHumjWle5Q/5Xbl8p/WL11jz1zd9ppKxGCqkmmB6ofZn7tVs9nTnwwu/+eJfp8XtlL9vOaG09uryfBy+DimH4RxT29OPbftZnXxb2iDiYlnDmps926H1eR+s33b+ccrd/7lYu9bf2JqYC0v5D6QEQ+WsB3zm/HM7RbGl67YXE6D13N7ialyc/gYwjWsVy8FCB55lz2HZOTvT8kxEwBiJTnWZ9OVC+b8fMBGmLI1oGYvRQPCssy0+mCIvt/3W1nj2ceUaa0a06Zacw+tADH/allTE+9L+OW50Zu3nLqcgg9T8suqVX3fBkxoccclsAbMAHlKv+aXvcTU5eTAswPKNKypTQAOJLPrzs4WcYVHBf+Kd9z5bHkhgjQlr9O4jm0togGSM5Tpde0uRJA6YNvhOLp3yvfY6Lkmc+3WgaA4Dh0Bd5GDODbD+SRpdyGOwB5QkJ1N1P7GXg+vfWq3Hgs4h5nfnWldgXzb4VNFDKzCXtiO1QfYbH6zu+ELW6cK8hFBNLbVBC13DlmgJFSdFYB9RO6dtVpUh4a466PyYFgNtJKFx8LKRXsCgpAzVXXMh62DZuZ4Zis7b6/YBagBal7ikpA9UFUJCklzrVhePTCtsaMHqgaNxLuqEkUrZamJQ5ghz2LHO+zIe8FJxBN4L+Kob3NQucfubsViuvXku5H7DlDGri0Bpyxgy0z/Bs3g1Cp7BT52WwuRZTC3lLuHGsRb3d8SkkIOoEzVUj91oMKy8u/OJUBPSCUElmFrOyII6NOqvmkDCUk0DdkMenmD/TwAVUrimRVYqnUSkr1giq5pZyHCQzMcMYvRW9gPc6B4rYf04ISm9NEMpAVToaaBMJ6WWK+4hCAWZAFyDrOSZE6Qvt0vIAvVIReN5mltgEY10wE3wmnKTTJY29VKLT0oSAIhBvil91AwDx7tOO30QyA1yTOYQhcYNasACVvpaQHxBWj2izZ9EE+EwbpW+pKQcwjTe2+CeQrTg8iF4ZNZH9jjCKLW0UH9dwFhdhC6oS1kSnniK/RqO+tIER12/KAybWEhQg0CMmvvvERN1HyBf41MC29vmsJIbygxVC0jRMRT9Cxg3NovRBgmDBea/ClrZqorwysLhGVbyJTCnXxPHUH31e2RsXR/9yTNlkBC+5IF5Bzuj4KNZiU+KMC7cra3PAoW1JtnSnksOqvV4f2ndUFLTkpXrC9qyoDg1psZ1w51gdcWE6vtD2a9ovis+srzojifDepHynPdmX3HPArobz9IlILrjK4mk8Hgz+RqFOcEdWmd7DvWCxFPHKU2BAaKFJTio0plQOfQrG8PNTjRVKw20GYzaxCGaVZoA+sBbTazpYE7DU3bkXHGxSwWDVt+bRcizkfYKXD0NNsEDT4Axy5QiyxT24UYOJTCzMyQ24iHAspKt2zMophwzpguRKhoDI1hvO69XeH+CEpomPSCgT2WJFEEn5xiaSnCHJZhphTkZTPbjBB01tRaOSOfZ+rP4ByWlaUPU3qZbQFB4KgWqyfeoafhNgQtcJbObCEGKiwsrSh8ZhKpi9AjcLiSZWwIta7wYlY1oHPIu+VVwSWAVqeO4aM1bStb8QZllCnl7bmfEprmZXEu2Wghogr99/f5WHU/UTQXq0632+lUgYhilp3rPeoHeG/Kst4/5qvlcHS4mP3J2+v6/X6b9WI5wJWqtPdBB7wkfuBLl7v580HbcX9w43PnP1a8394wdJKqSaY0rs7Ju3zd+PGj2D/SKnlphALRxRU+nzfyb/qzvQ2nO4+wKFmA1V6hV5zfy/fkYSYoFWNFYgoEjtUM4DbSIcxE97fIlLIzVWpesuysis5cdnuLkgVQK1gzCIGI3RthMaNFrkTeQiFw4+Jrb/4Rl9GTgNq3a/BZ7Cie5JXv+gsRZklqcIu4ewfswf33Vu8pbfRvc5GmB47/7Ee/pxRmK+uI846bfMLt29P2goP/u7B/GDGrBWYn6m+tHOLjx41EERUPF/abfqHteY8a/jFLUX/rQAivDnRMxSFI67b/l7B+TxR31H7hdBdirXMYkrDeyxB3LnyhvBAXDRT6h4SG31C1ZKGDA/oREta7GOBUrSh0M6UNmtG+JIy4d9N/H1Y9ZUHmHH4TteU322qVm7ub/n+uf+DnmrM/cXpoKKHS6dqfsHP+okU85I+4alBtZcNFGEdGtZWt6Se0xSmqmtP8K3LF6qjGm4UtikmhZh6cOYpWTVPT0RjFSp4T/YaK2QtJRL9FNDfE05RQM/Dd1Di2RTOQgcswjo1mwULTIIYt+NjbZsiO0GsJP9cspW0W07RFN57YFRxl2Rq66Sfc03VMCu3s06Sxp2pDoenhfzLundBndCbFZtVTmZ/EvuidezXqYukOV73cubL0x5mx3pely8v5m+JGyJiOB8+zl/mmd94675Vuq+X1pJVjeKqq6hyBqvW/uUgkEolEIpFIJBKJRCKRSCQSiUQikUjE8h/PNKFDoWQnRwAAAABJRU5ErkJggg==";
        
                    //questo snippet mi aggiorna il movimento più recente effettuato 
                    const Day = new Date();
                    let dayU = Day.getDate();
                    let monthU = Day.getMonth() +1;
                    let yearU = Day.getFullYear();

                    document.getElementById("titU").innerHTML= dayU+" " +"/"+" "+ monthU+" " + "/"+" "+ yearU;
                    document.getElementById("descrU").innerHTML= "Prelievo contanti <br>" + "Carta 1234***************2345";
                    document.getElementById("costU").innerHTML= "-"+ " "+ importoPrelevato +" " + "€" ; //Se impostassi una condizione sull'importo prelevato?
                    document.getElementById("imU").src= "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8XuXgAtnL4/fsPu3ua3b+e3cAAtW8AtW4At3T1/PkZvH0Jt3X7/v3m9u+f3sLI69uu48vd8+rO7uCS2bm45tFKw47X8ebt+vXj9u510alszaJTx5U6wYi+6daV27xgypuD1bB90qtaypqx48xNxpFx0KZnyp05wosyvoKL2LZuky4qAAAJsklEQVR4nO2da1fqOhCGbVDbQgoCoiiiCNst+P9/4MHLPsrMG5opMwXXyvO5tJk2mcw1nJ0lEolEIpFIJBKJRCKRSCQSiUQikUgkToCq8ztoKt94uT7/HayX4ybfb5Y7n/0OvMtnlVjCB3fscYtwD1IBL/Jjj1lIfiETsNM79ojF9GQKZ1wce8Bicpm2Gf6uVfiOG4okHPxCCQciCS9/oYSXScIk4amTJEwSnj5JwiTh6ZMkTBKePmoSuiMTjB2pSXhxXAY3IRGVJCxfRLcxIOibK0noro0GHs1daSthfmU08HjmgWmqJGHRNRp3PNeh6aUiod8YDVtAKAyoI6FbGg1bgukszWUROxuWgbevImFxZzRqCY844aAjoTBybkMX7xcqEvpXo0HLeIUiqkjonozGLGMIp6mKhMLkhxXYrFGR8LxvNGYh0PrWkNDfGI1YCjRrNCR097sXdQbMuZloCNB/orclKnxsJWFOxn9XUPe0ULF5+uy2t+QKtIRUvuFo96IJ02lOmEwPsKkb/D0YnoqmIRe9sYvoV27IiijLku7DyKxRkNCvyEV852XTqRns1fVItUzH5hvSNHK1YUq71NlO2PQvyPo4W/D9QkHCguz3Xe6obXQc5FsqIatCAGaNgoQ5mSpX7ClqGyZ9d2wh9vn4hBKCWgw2fH6NXxwi1g96dBJ6egU3a4S1GHwjyNwbueYvUzTu+RCxfsB0GFuI3KwR6vE+l5Ddgb1o6WsMc0HH7/6QK265hEItR7ek7UPoHbiiUYs1silUzsgVFbUKSrqX1cHekZ8T4/CWS+h0tsN3e5A+nQX56BqRP3vidm/B3iJYql7Lu+qe01uXdCHumjWle5Q/5Xbl8p/WL11jz1zd9ppKxGCqkmmB6ofZn7tVs9nTnwwu/+eJfp8XtlL9vOaG09uryfBy+DimH4RxT29OPbftZnXxb2iDiYlnDmps926H1eR+s33b+ccrd/7lYu9bf2JqYC0v5D6QEQ+WsB3zm/HM7RbGl67YXE6D13N7ialyc/gYwjWsVy8FCB55lz2HZOTvT8kxEwBiJTnWZ9OVC+b8fMBGmLI1oGYvRQPCssy0+mCIvt/3W1nj2ceUaa0a06Zacw+tADH/allTE+9L+OW50Zu3nLqcgg9T8suqVX3fBkxoccclsAbMAHlKv+aXvcTU5eTAswPKNKypTQAOJLPrzs4WcYVHBf+Kd9z5bHkhgjQlr9O4jm0togGSM5Tpde0uRJA6YNvhOLp3yvfY6Lkmc+3WgaA4Dh0Bd5GDODbD+SRpdyGOwB5QkJ1N1P7GXg+vfWq3Hgs4h5nfnWldgXzb4VNFDKzCXtiO1QfYbH6zu+ELW6cK8hFBNLbVBC13DlmgJFSdFYB9RO6dtVpUh4a466PyYFgNtJKFx8LKRXsCgpAzVXXMh62DZuZ4Zis7b6/YBagBal7ikpA9UFUJCklzrVhePTCtsaMHqgaNxLuqEkUrZamJQ5ghz2LHO+zIe8FJxBN4L+Kob3NQucfubsViuvXku5H7DlDGri0Bpyxgy0z/Bs3g1Cp7BT52WwuRZTC3lLuHGsRb3d8SkkIOoEzVUj91oMKy8u/OJUBPSCUElmFrOyII6NOqvmkDCUk0DdkMenmD/TwAVUrimRVYqnUSkr1giq5pZyHCQzMcMYvRW9gPc6B4rYf04ISm9NEMpAVToaaBMJ6WWK+4hCAWZAFyDrOSZE6Qvt0vIAvVIReN5mltgEY10wE3wmnKTTJY29VKLT0oSAIhBvil91AwDx7tOO30QyA1yTOYQhcYNasACVvpaQHxBWj2izZ9EE+EwbpW+pKQcwjTe2+CeQrTg8iF4ZNZH9jjCKLW0UH9dwFhdhC6oS1kSnniK/RqO+tIER12/KAybWEhQg0CMmvvvERN1HyBf41MC29vmsJIbygxVC0jRMRT9Cxg3NovRBgmDBea/ClrZqorwysLhGVbyJTCnXxPHUH31e2RsXR/9yTNlkBC+5IF5Bzuj4KNZiU+KMC7cra3PAoW1JtnSnksOqvV4f2ndUFLTkpXrC9qyoDg1psZ1w51gdcWE6vtD2a9ovis+srzojifDepHynPdmX3HPArobz9IlILrjK4mk8Hgz+RqFOcEdWmd7DvWCxFPHKU2BAaKFJTio0plQOfQrG8PNTjRVKw20GYzaxCGaVZoA+sBbTazpYE7DU3bkXHGxSwWDVt+bRcizkfYKXD0NNsEDT4Axy5QiyxT24UYOJTCzMyQ24iHAspKt2zMophwzpguRKhoDI1hvO69XeH+CEpomPSCgT2WJFEEn5xiaSnCHJZhphTkZTPbjBB01tRaOSOfZ+rP4ByWlaUPU3qZbQFB4KgWqyfeoafhNgQtcJbObCEGKiwsrSh8ZhKpi9AjcLiSZWwIta7wYlY1oHPIu+VVwSWAVqeO4aM1bStb8QZllCnl7bmfEprmZXEu2Wghogr99/f5WHU/UTQXq0632+lUgYhilp3rPeoHeG/Kst4/5qvlcHS4mP3J2+v6/X6b9WI5wJWqtPdBB7wkfuBLl7v580HbcX9w43PnP1a8394wdJKqSaY0rs7Ju3zd+PGj2D/SKnlphALRxRU+nzfyb/qzvQ2nO4+wKFmA1V6hV5zfy/fkYSYoFWNFYgoEjtUM4DbSIcxE97fIlLIzVWpesuysis5cdnuLkgVQK1gzCIGI3RthMaNFrkTeQiFw4+Jrb/4Rl9GTgNq3a/BZ7Cie5JXv+gsRZklqcIu4ewfswf33Vu8pbfRvc5GmB47/7Ee/pxRmK+uI846bfMLt29P2goP/u7B/GDGrBWYn6m+tHOLjx41EERUPF/abfqHteY8a/jFLUX/rQAivDnRMxSFI67b/l7B+TxR31H7hdBdirXMYkrDeyxB3LnyhvBAXDRT6h4SG31C1ZKGDA/oREta7GOBUrSh0M6UNmtG+JIy4d9N/H1Y9ZUHmHH4TteU322qVm7ub/n+uf+DnmrM/cXpoKKHS6dqfsHP+okU85I+4alBtZcNFGEdGtZWt6Se0xSmqmtP8K3LF6qjGm4UtikmhZh6cOYpWTVPT0RjFSp4T/YaK2QtJRL9FNDfE05RQM/Dd1Di2RTOQgcswjo1mwULTIIYt+NjbZsiO0GsJP9cspW0W07RFN57YFRxl2Rq66Sfc03VMCu3s06Sxp2pDoenhfzLundBndCbFZtVTmZ/EvuidezXqYukOV73cubL0x5mx3pely8v5m+JGyJiOB8+zl/mmd94675Vuq+X1pJVjeKqq6hyBqvW/uUgkEolEIpFIJBKJRCKRSCQSiUQikUjE8h/PNKFDoWQnRwAAAABJRU5ErkJggg==";
                    } 
     }  else {
                if (document.getElementById("no-agg")!= null) {
                    document.getElementById("no-agg").innerHTML = "Non ci sono aggiornamenti";
                } 
            }    



      
    
    






























 
 
    

 